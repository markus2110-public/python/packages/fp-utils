# Python Functional Programming Utils

## Preamble
> In computer science, functional programming is a programming paradigm where programs are constructed by applying and composing functions.   
> It is a declarative programming paradigm in which function definitions are trees of expressions that map values to other values,   
> rather than a sequence of imperative statements which update the running state of the program.    
> [`Source: https://en.wikipedia.org/wiki/Functional_programming`](https://en.wikipedia.org/wiki/Functional_programming)
***

## Description
This package contains currently two common function, which are often used in functional programming.   
The 
- `curry` function,
- `compose` and `pipe` function, which are pretty much the same.

> The `curry` function turns a function with arguments in a sequence of function, 
> where each function only accept one argument.

***
> The `compose` or `pipe` function is used to create a new function from multiple smaller function.
> Each function has only one purpose, and therefore the function is better testable.
> The `compose` or `pipe` function is a function that accept one or more functions as arguments and returns a new function
> that combines the functionality of all given functions.   
> `It composes a new function of the given function`
> ***
> The difference between `compose` and `pipe` is only how to assign the function.   
> `compose` performs right-to-left function composition, where     
> `pipe` performs left-to-right function composition

I recommend to us the `pipe` function, it makes the code much better readable.   
Normally we read text from left to right, why should I now start to read a compose function from right to left   
to understand what is the outcome of the function 
***

## Installation
```commandline
pip install sat_fp_utils
```
or
```commandline
python -m pip install sat_fp_utils
```
***

## Usage
> The `curry` function turns a function with arguments in a sequence of function, 
> where each function only accept one argument.

```python
from sat_fp_utils.curry import curry

def my_func(a, b, c):
    return a + b + c

new_func = curry(my_func)
```
Now you can use the function as follows
```python
result = new_func(a)(b)(c)
```   
or   
```python
result = new_func(a, b)(c)
```   
or   
```python
func_a = new_func(a)
func_b = func_a(b)
result = func_b(c)
```

***
> The `compose` or `pipe` function is used to create a new function from multiple smaller function.
> Each function has only one purpose, and therefore the function is better testable.
> The `compose` or `pipe` function is a function that accept one or more functions as arguments and returns a new function
> that combines the functionality of all given functions.   
> `It composes a new function of the given function`

```python
from sat_fp_utils.composition import pipe, compose

def f_add1(value: int) -> int :
    return value + 1

def f_add2(value: int) -> int :
    return value + 2

def f_add3(value: int) -> int :
    return value + 3

pipe_function = pipe(f_add1, f_add2, f_add3)
result = pipe_function(1)
> 1 + 1 + 2 + 3 = 7

compose_function = compose(f_add3, f_add2, f_add1)
result = compose_function(1)
> 1 + 3 + 2 + 1 = 7
```


*** 
## Example

- [ ] Add real world examples here 






