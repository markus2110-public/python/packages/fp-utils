from fp_utils.curry import curry


def f1(a, b, c):
    return a + b + c


if __name__ == '__main__':
    add = curry(f1)

    add5 = add(5)

    add25 = add5(20)
    print(add25(25))
