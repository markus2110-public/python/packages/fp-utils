from functools import reduce
from sat_fp_utils.helper import reverse


def compose(*funcs):
    """
    Performs right-to-left function composition.
    The last argument may have any arity;
    the remaining arguments must be unary.

    :param funcs: The functions to compose together
    :return: A new function
    """
    def creator(outer_func, inner_func):
        return lambda value: outer_func(inner_func(value))
    return reduce(creator, funcs)


def pipe(*funcs):
    """
    Performs left-to-right function composition.
    The first argument may have any arity;
    the remaining arguments must be unary.

    :param funcs: The functions to pipe together
    :return: A new function
    """
    rfuncs = reverse(funcs)

    def creator(outer_func, inner_func):
        return lambda value: outer_func(inner_func(value))
    return reduce(creator, rfuncs)
