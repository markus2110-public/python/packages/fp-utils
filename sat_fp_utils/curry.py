from typing import Callable


def curry(func: Callable) -> Callable:
    """

    :param func:
    :return:
    """
    if not callable(func):
        raise ValueError("Parameter <func> must be a function!")

    total_function_args = func.__code__.co_varnames
    uses_args = []

    def func_wrapper(*args):
        nonlocal uses_args
        uses_args += args
        if len(uses_args) == len(total_function_args):
            return func(*uses_args)
        else:
            return func_wrapper

    return func_wrapper
