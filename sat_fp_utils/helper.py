from typing import Dict, Callable
from inspect import signature, Signature, Parameter


def func_name(func: Callable) -> str:
    """

    :param func: The function to get the name from
    :return: The function name
    """
    return func.__name__


def func_signature(func: Callable) -> Signature:
    """Get a signature object for the passed callable."""
    return signature(func)


def func_params(func: Callable) -> Dict[str, Parameter]:
    """

    :param func:
        The function to get the defined arguments from
    :return:
        Returns an <b>dict()</b> with the Attribute name as key
        and an instance of <b>ParameterModel</b> as value
    """

    sig = func_signature(func)
    parameter_dict = dict()
    for item in sig.parameters:
        parameter = sig.parameters.get(item)
        parameter_dict[item] = parameter

    return parameter_dict


def reverse(value):
    """
    :param value:
        The value which we want to reverse
    :return:
        Returns the given value in reverse order
    """
    if type(value) is dict:
        raise ValueError("Value of type <Dict> currently not reversible !")

    return value[::-1]
