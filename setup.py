import setuptools

# required packages
requirements = []
# required build packages
build_requirements = ['build', 'twine']
# required test packages
test_requirements = ['flake8', 'coverage', 'tox']
# required dev packages
dev_requirements = []


with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()


setuptools.setup(
    name="sat_fp_utils",
    version="0.0.6",
    author="Markus",
    author_email="markus2110@gmail.com",
    description="Package for functional programming utils",
    long_description=long_description,
    long_description_content_type="text/markdown",
    keywords=[
        "programming",
        "functional",
        "curry",
        "compose",
        "pipe",
        "function"
    ],
    url="https://gitlab.com/markus2110-public/python/packages/fp-utils",
    packages=setuptools.find_packages(),
    install_requires=requirements,
    extras_require={
        'build': requirements + build_requirements,
        'testing': requirements + test_requirements,
        'dev': requirements + build_requirements + test_requirements + dev_requirements,
    },
    classifiers=[
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
        "Programming Language :: Python :: 3.12",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Topic :: Utilities"
    ],
    python_requires='>=3.8,<3.13',
)
