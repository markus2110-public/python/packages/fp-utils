from unittest import TestCase
from sat_fp_utils import compose, pipe


def double(value: int) -> int:
    return value * 2


def sqare(value: int) -> int:
    return value**2


def add2(value: int) -> int:
    return value + 2


class TestFunctionComposition(TestCase):

    def test_compose(self):
        """ 2*2 -> 4**2 -> 16+2 = 32 """
        expected_result = 32

        result = compose(double, sqare, add2)(2)
        self.assertEqual(result, expected_result)

    def test_compose_should_return_a_function(self):
        result = compose(double, sqare, add2)
        self.assertTrue(callable(result))

    def test_pipe(self):
        """ 2*2 -> 4**2 -> 16+2 = 18 """
        expected_result = 18

        result = pipe(double, sqare, add2)(2)
        self.assertEqual(result, expected_result)

    def test_pipe_should_return_a_function(self):
        result = compose(double, sqare, add2)
        self.assertTrue(callable(result))
