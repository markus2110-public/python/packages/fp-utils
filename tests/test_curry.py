from unittest import TestCase
from sat_fp_utils.curry import curry


def my_func(a="a", b="b", c="c"):
    return a + b + c


class TestCurry(TestCase):

    def test_curry_raises_a_error_if_no_function_is_given(self):
        with self.assertRaises(ValueError) as error:
            curry("a string")

        expected_msg = "Parameter <func> must be a function!"
        current_msg = str(error.exception)
        self.assertEqual(expected_msg, current_msg)

    def test_curry_returns_a_function(self):
        add = curry(my_func)

        result = add(2)(2)(2)
        self.assertEqual(result, 6)

        f = curry(my_func)
        add_5 = f(5)
        add_10 = add_5(5)
        result = add_10(10)
        self.assertEqual(result, 20)

        f = curry(my_func)
        add_10 = f(5, 5)
        result = add_10(10)
        self.assertEqual(result, 20)

        f = curry(my_func)
        self.assertEqual(f(10, 10, 10), 30)
