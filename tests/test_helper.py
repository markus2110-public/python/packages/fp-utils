from unittest import TestCase
from inspect import Parameter
from sat_fp_utils import reverse, func_name, func_params


def my_func_name(a, b, c="demo"):
    pass


class TestFunctionSignatures(TestCase):
    def test_function_name(self):
        expected_name = "my_func_name"
        self.assertEqual(expected_name, func_name(my_func_name))

    def test_function_params(self):
        result = func_params(my_func_name)

        first_arg = result.get("a")
        last_arg = result.get("c")

        self.assertEqual(first_arg.name, "a")
        self.assertTrue(first_arg.default, Parameter.empty)

        self.assertEqual(last_arg.name, "c")
        self.assertEqual(last_arg.default, "demo")


class TestReverseHelper(TestCase):

    def test_reverse_a_string(self):
        given_val = "Hello World"
        expected = "dlroW olleH"
        self.assertEqual(expected, reverse(given_val))

    def test_reverse_a_list(self):
        given_val = [1, 2, 3]
        expected = [3, 2, 1]
        self.assertEqual(expected, reverse(given_val))

    def test_reverse_a_tuple(self):
        given_val = tuple((1, 2, 3))
        expected = tuple((3, 2, 1))
        self.assertEqual(expected, reverse(given_val))

    def test_reverse_a_dict(self):
        given_val = dict({1: "A", 2: "B"})

        with self.assertRaises(ValueError):
            reverse(given_val)
