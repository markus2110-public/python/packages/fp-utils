from unittest import TestCase
from inspect import Parameter
from sat_fp_utils.curry import curry



class TestReadmeExamples(TestCase):

    def test_curry_example1(self):
        def f1(a, b, c):
            return a + b + c

        curry_f1 = curry(f1)

        # test the normal function
        self.assertEqual(6, f1(1, 2, 3))
        # test the curry with all arguments
        self.assertEqual(6, curry_f1(1, 2, 3))

        # # test the curry as a sequence of functions
        self.assertEqual(6, curry_f1(1)(2)(3))
        # # test the curry as a sequence of functions 1
        # self.assertEqual(6, curry_f1(1, 2)(3))
        # # test the curry as a sequence of functions 1
        # self.assertEqual(6, curry_f1(1)(2, 3))

